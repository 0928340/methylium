﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotAI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
            currentWindow = "Startup";
            loadPage();
        }
        //Strings
        string currentWindow = "";

        //Page Loaders
        private void loadPage()
        {
            if (currentWindow == "Startup")
            {
                pageContainer.Controls.Clear();
                pageContainer.Dock = DockStyle.Fill;
                startScreen startScreendwindow = new startScreen();
                startScreendwindow.Dock = DockStyle.Fill;
                pageContainer.Controls.Add(startScreendwindow);

            }
            if (currentWindow == "SoloClicked")
            {
                pageContainer.Controls.Clear();
                pageContainer.Dock = DockStyle.Fill;
                startScreen startScreendwindow = new startScreen();
                startScreendwindow.Dock = DockStyle.Fill;
                pageContainer.Controls.Add(startScreendwindow);

            }
        }
            private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Button 1 clicked!");
            if (!pageContainer.Controls.Contains(startScreen.Instance))
            {
                pageContainer.Controls.Add(startScreen.Instance);
                startScreen.Instance.Dock = DockStyle.Fill;
                startScreen.Instance.BringToFront();
            }
            else
            {
                startScreen.Instance.BringToFront();
            }
        }
    }
}
