﻿namespace HotAI0._0._1
{
    partial class scanScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Help = new System.Windows.Forms.Label();
            this.btn_About = new System.Windows.Forms.Label();
            this.btn_Settings = new System.Windows.Forms.Label();
            this.lbl_groupInfo = new System.Windows.Forms.Label();
            this.btn_TakePhoto = new System.Windows.Forms.PictureBox();
            this.pb_liveCamera = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btn_TakePhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_liveCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Help
            // 
            this.btn_Help.AutoSize = true;
            this.btn_Help.BackColor = System.Drawing.Color.White;
            this.btn_Help.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Help.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_Help.Location = new System.Drawing.Point(948, 532);
            this.btn_Help.Name = "btn_Help";
            this.btn_Help.Size = new System.Drawing.Size(35, 15);
            this.btn_Help.TabIndex = 1;
            this.btn_Help.Text = "Help";
            this.btn_Help.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_About
            // 
            this.btn_About.AutoSize = true;
            this.btn_About.BackColor = System.Drawing.Color.White;
            this.btn_About.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_About.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_About.Location = new System.Drawing.Point(818, 532);
            this.btn_About.Name = "btn_About";
            this.btn_About.Size = new System.Drawing.Size(44, 15);
            this.btn_About.TabIndex = 2;
            this.btn_About.Text = "About";
            this.btn_About.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_Settings
            // 
            this.btn_Settings.AutoSize = true;
            this.btn_Settings.BackColor = System.Drawing.Color.White;
            this.btn_Settings.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Settings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_Settings.Location = new System.Drawing.Point(877, 532);
            this.btn_Settings.Name = "btn_Settings";
            this.btn_Settings.Size = new System.Drawing.Size(52, 15);
            this.btn_Settings.TabIndex = 3;
            this.btn_Settings.Text = "Settings";
            this.btn_Settings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_groupInfo
            // 
            this.lbl_groupInfo.AutoSize = true;
            this.lbl_groupInfo.BackColor = System.Drawing.Color.White;
            this.lbl_groupInfo.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_groupInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.lbl_groupInfo.Location = new System.Drawing.Point(18, 533);
            this.lbl_groupInfo.Name = "lbl_groupInfo";
            this.lbl_groupInfo.Size = new System.Drawing.Size(247, 15);
            this.lbl_groupInfo.TabIndex = 4;
            this.lbl_groupInfo.Text = "Hogeschool Rotterdam - HotAI - Project D";
            // 
            // btn_TakePhoto
            // 
            this.btn_TakePhoto.Image = global::HotAI0._0._1.Properties.Resources.btn_takephoto;
            this.btn_TakePhoto.Location = new System.Drawing.Point(452, 443);
            this.btn_TakePhoto.Name = "btn_TakePhoto";
            this.btn_TakePhoto.Size = new System.Drawing.Size(127, 34);
            this.btn_TakePhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btn_TakePhoto.TabIndex = 7;
            this.btn_TakePhoto.TabStop = false;
            this.btn_TakePhoto.Click += new System.EventHandler(this.button1_Click_1Async);
            this.btn_TakePhoto.MouseEnter += new System.EventHandler(this.btn_TakePhoto_MouseEnter);
            this.btn_TakePhoto.MouseLeave += new System.EventHandler(this.btn_TakePhoto_MouseLeave);
            // 
            // pb_liveCamera
            // 
            this.pb_liveCamera.Location = new System.Drawing.Point(81, 87);
            this.pb_liveCamera.Name = "pb_liveCamera";
            this.pb_liveCamera.Size = new System.Drawing.Size(498, 350);
            this.pb_liveCamera.TabIndex = 5;
            this.pb_liveCamera.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HotAI0._0._1.Properties.Resources.ScanScreen;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1000, 559);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // scanScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_TakePhoto);
            this.Controls.Add(this.pb_liveCamera);
            this.Controls.Add(this.btn_Help);
            this.Controls.Add(this.btn_About);
            this.Controls.Add(this.btn_Settings);
            this.Controls.Add(this.lbl_groupInfo);
            this.Controls.Add(this.pictureBox1);
            this.Name = "scanScreen";
            this.Size = new System.Drawing.Size(1000, 559);
            ((System.ComponentModel.ISupportInitialize)(this.btn_TakePhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_liveCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label btn_Help;
        private System.Windows.Forms.Label btn_About;
        private System.Windows.Forms.Label btn_Settings;
        private System.Windows.Forms.Label lbl_groupInfo;
        private System.Windows.Forms.PictureBox pb_liveCamera;
        private System.Windows.Forms.PictureBox btn_TakePhoto;
    }
}
