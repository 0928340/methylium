﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotAI
{
    public partial class startScreen : UserControl
    {
        private static startScreen _instance;
        public static startScreen Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new startScreen();
                return _instance;
            }
        }

        public startScreen()
        {
            InitializeComponent();
        }

        private void startScreen_Load(object sender, EventArgs e)
        {

        }
    }
}
