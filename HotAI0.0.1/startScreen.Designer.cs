﻿namespace HotAI
{
    partial class startScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_groupInfo = new System.Windows.Forms.Label();
            this.btn_Settings = new System.Windows.Forms.Label();
            this.btn_Help = new System.Windows.Forms.Label();
            this.btn_About = new System.Windows.Forms.Label();
            this.pb_btnSolo = new System.Windows.Forms.PictureBox();
            this.pb_btnGroup = new System.Windows.Forms.PictureBox();
            this.btn_Solo = new System.Windows.Forms.Label();
            this.btn_Group = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_btnSolo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_btnGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_groupInfo
            // 
            this.lbl_groupInfo.AutoSize = true;
            this.lbl_groupInfo.BackColor = System.Drawing.Color.Transparent;
            this.lbl_groupInfo.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_groupInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.lbl_groupInfo.Location = new System.Drawing.Point(18, 533);
            this.lbl_groupInfo.Name = "lbl_groupInfo";
            this.lbl_groupInfo.Size = new System.Drawing.Size(247, 15);
            this.lbl_groupInfo.TabIndex = 0;
            this.lbl_groupInfo.Text = "Hogeschool Rotterdam - HotAI - Project D";
            // 
            // btn_Settings
            // 
            this.btn_Settings.AutoSize = true;
            this.btn_Settings.BackColor = System.Drawing.Color.Transparent;
            this.btn_Settings.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Settings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_Settings.Location = new System.Drawing.Point(876, 533);
            this.btn_Settings.Name = "btn_Settings";
            this.btn_Settings.Size = new System.Drawing.Size(52, 15);
            this.btn_Settings.TabIndex = 0;
            this.btn_Settings.Text = "Settings";
            this.btn_Settings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Settings.MouseEnter += new System.EventHandler(this.btn_Settings_MouseEnter);
            this.btn_Settings.MouseLeave += new System.EventHandler(this.btn_Settings_MouseLeave);
            // 
            // btn_Help
            // 
            this.btn_Help.AutoSize = true;
            this.btn_Help.BackColor = System.Drawing.Color.Transparent;
            this.btn_Help.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Help.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_Help.Location = new System.Drawing.Point(947, 533);
            this.btn_Help.Name = "btn_Help";
            this.btn_Help.Size = new System.Drawing.Size(35, 15);
            this.btn_Help.TabIndex = 0;
            this.btn_Help.Text = "Help";
            this.btn_Help.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btn_Help.MouseEnter += new System.EventHandler(this.btn_Help_MouseEnter);
            this.btn_Help.MouseLeave += new System.EventHandler(this.btn_Help_MouseLeave);
            // 
            // btn_About
            // 
            this.btn_About.AutoSize = true;
            this.btn_About.BackColor = System.Drawing.Color.Transparent;
            this.btn_About.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_About.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_About.Location = new System.Drawing.Point(817, 533);
            this.btn_About.Name = "btn_About";
            this.btn_About.Size = new System.Drawing.Size(44, 15);
            this.btn_About.TabIndex = 0;
            this.btn_About.Text = "About";
            this.btn_About.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_About.MouseEnter += new System.EventHandler(this.btn_About_MouseEnter);
            this.btn_About.MouseLeave += new System.EventHandler(this.btn_About_MouseLeave);
            // 
            // pb_btnSolo
            // 
            this.pb_btnSolo.Image = global::HotAI0._0._1.Properties.Resources.buttonNormal;
            this.pb_btnSolo.Location = new System.Drawing.Point(220, 304);
            this.pb_btnSolo.Name = "pb_btnSolo";
            this.pb_btnSolo.Size = new System.Drawing.Size(120, 34);
            this.pb_btnSolo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_btnSolo.TabIndex = 1;
            this.pb_btnSolo.TabStop = false;
            this.pb_btnSolo.Click += new System.EventHandler(this.pb_btnSolo_Click);
            this.pb_btnSolo.MouseEnter += new System.EventHandler(this.pb_btnSolo_MouseEnter);
            this.pb_btnSolo.MouseLeave += new System.EventHandler(this.pb_btnSolo_MouseLeave);
            // 
            // pb_btnGroup
            // 
            this.pb_btnGroup.Image = global::HotAI0._0._1.Properties.Resources.buttonNormal;
            this.pb_btnGroup.Location = new System.Drawing.Point(661, 304);
            this.pb_btnGroup.Name = "pb_btnGroup";
            this.pb_btnGroup.Size = new System.Drawing.Size(120, 34);
            this.pb_btnGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_btnGroup.TabIndex = 2;
            this.pb_btnGroup.TabStop = false;
            this.pb_btnGroup.Click += new System.EventHandler(this.pb_btnGroup_Click);
            this.pb_btnGroup.MouseEnter += new System.EventHandler(this.pb_btnGroup_MouseEnter);
            this.pb_btnGroup.MouseLeave += new System.EventHandler(this.pb_btnGroup_MouseLeave);
            // 
            // btn_Solo
            // 
            this.btn_Solo.AutoSize = true;
            this.btn_Solo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(129)))), ((int)(((byte)(191)))));
            this.btn_Solo.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Solo.ForeColor = System.Drawing.Color.White;
            this.btn_Solo.Location = new System.Drawing.Point(256, 311);
            this.btn_Solo.Name = "btn_Solo";
            this.btn_Solo.Size = new System.Drawing.Size(36, 18);
            this.btn_Solo.TabIndex = 0;
            this.btn_Solo.Text = "Solo";
            this.btn_Solo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Solo.Click += new System.EventHandler(this.btn_Solo_Click);
            this.btn_Solo.MouseEnter += new System.EventHandler(this.btn_Solo_MouseEnter);
            this.btn_Solo.MouseLeave += new System.EventHandler(this.btn_About_MouseLeave);
            // 
            // btn_Group
            // 
            this.btn_Group.AutoSize = true;
            this.btn_Group.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(129)))), ((int)(((byte)(191)))));
            this.btn_Group.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Group.ForeColor = System.Drawing.Color.White;
            this.btn_Group.Location = new System.Drawing.Point(688, 311);
            this.btn_Group.Name = "btn_Group";
            this.btn_Group.Size = new System.Drawing.Size(51, 18);
            this.btn_Group.TabIndex = 0;
            this.btn_Group.Text = "Group";
            this.btn_Group.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Group.MouseEnter += new System.EventHandler(this.btn_Group_MouseEnter);
            this.btn_Group.MouseLeave += new System.EventHandler(this.btn_About_MouseLeave);
            // 
            // startScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.BackgroundImage = global::HotAI0._0._1.Properties.Resources.StartScreen;
            this.Controls.Add(this.btn_Group);
            this.Controls.Add(this.btn_Solo);
            this.Controls.Add(this.pb_btnGroup);
            this.Controls.Add(this.btn_Help);
            this.Controls.Add(this.btn_About);
            this.Controls.Add(this.btn_Settings);
            this.Controls.Add(this.lbl_groupInfo);
            this.Controls.Add(this.pb_btnSolo);
            this.Name = "startScreen";
            this.Size = new System.Drawing.Size(1000, 559);
            ((System.ComponentModel.ISupportInitialize)(this.pb_btnSolo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_btnGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_groupInfo;
        private System.Windows.Forms.Label btn_Settings;
        private System.Windows.Forms.Label btn_Help;
        private System.Windows.Forms.Label btn_About;
        private System.Windows.Forms.PictureBox pb_btnSolo;
        private System.Windows.Forms.PictureBox pb_btnGroup;
        private System.Windows.Forms.Label btn_Solo;
        private System.Windows.Forms.Label btn_Group;
    }
}
