﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using HotAI;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Azure.CognitiveServices.Vision.Face;
using Microsoft.Azure.CognitiveServices.Vision.Face.Models;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;

namespace HotAI0._0._1
{
    public partial class scanScreen : UserControl
    {
        private static scanScreen _instance;
        public static scanScreen Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new scanScreen();
                return _instance;
            }
        }

        //Appconfig settings

        const string subscriptionKey = "73457b283d934088b5c0b7c5732265c3";
        const string uriBase = "https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect";
        const string uriBaseVerify = "https://westeurope.api.cognitive.microsoft.com/face/v1.0/verify";
        List<List<FaceDescription>> facesDetected = new List<List<FaceDescription>>();

        private const string faceEndpoint =
           "https://westeurope.api.cognitive.microsoft.com";

        private readonly IFaceClient faceClient = new FaceClient(
            new ApiKeyServiceClientCredentials(subscriptionKey),
            new System.Net.Http.DelegatingHandler[] { });

        // The list of detected faces.
        private IList<DetectedFace> faceList;
        // The list of descriptions for the detected faces.
        private FaceDescription[] faceDescriptions;

        private bool _streaming;
        VideoCapture capture;

        public scanScreen()
        {
            InitializeComponent();

            _streaming = false;
            capture = new VideoCapture();

            if (Uri.IsWellFormedUriString(faceEndpoint, UriKind.Absolute))
            {
                faceClient.Endpoint = faceEndpoint;
            }
            else
            {
                MessageBox.Show(faceEndpoint,
                    "Invalid URI");
                Environment.Exit(0);
            }
            startStreaming();
        }

        private async void button1_Click_1Async(object sender, EventArgs e)
        {
            {
                faceList = await UploadAndDetectFaces(pb_liveCamera.Image);
                if (faceList.Count > 0)
                {
                    MessageBox.Show("Succes count=" + faceList.Count);
                    faceDescriptions = new FaceDescription[faceList.Count];

                    for (int i = 0; i < faceList.Count; ++i)
                    {
                        DetectedFace face = faceList[i];
                        faceDescriptions[i] = getFaceDescription(face);
                        facesDetected.Add(new List<FaceDescription> { faceDescriptions[i] });


                        MessageBox.Show("Person: " + (i + 1) + " - Age: " + faceList[i].FaceAttributes.Age
                            + " - Gender : " + faceList[i].FaceAttributes.Gender);
                    }

                }
                else
                {
                    MessageBox.Show("Fail");
                }
            }

        }


        // Uploads the image file and calls DetectWithStreamAsync.
        private async Task<IList<DetectedFace>> UploadAndDetectFaces(Image image)
        {
            // The list of Face attributes to return.
            IList<FaceAttributeType> faceAttributes =
                new FaceAttributeType[]
                {
                    FaceAttributeType.Gender, FaceAttributeType.Age,
                    FaceAttributeType.Smile, FaceAttributeType.Emotion,
                    FaceAttributeType.Glasses, FaceAttributeType.Hair
                };

            // Call the Face API.
            try
            {
                //GET STREAM
                var stream = ToStream(image, ImageFormat.Jpeg);

                // The second argument specifies to return the faceId, while
                // the third argument specifies not to return face landmarks.
                IList<DetectedFace> faceList =
                        await faceClient.Face.DetectWithStreamAsync(
                            stream, true, false, faceAttributes);
                return faceList;

            }
            // Catch and display Face API errors.
            catch (APIErrorException f)
            {
                MessageBox.Show(f.Message);
                return new List<DetectedFace>();
            }
            // Catch and display all other errors.
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error");
                return new List<DetectedFace>();
            }
        }

        public Stream ToStream(Image image, ImageFormat format)
        {
            var stream = new MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }
        // Creates a string out of the attributes describing the face.
        private FaceDescription getFaceDescription(DetectedFace face)
        {
            Guid faceg = (Guid)face.FaceId;
            FaceDescription faceDescription = new FaceDescription(faceg,
                Int32.Parse(face.FaceAttributes.Age.ToString()),
                face.FaceAttributes.Gender.ToString());

            return faceDescription;
        }



        private void startStreaming()
        {
            if (!_streaming)
            {
                Application.Idle += streaming;
            }
            else
            {
                Application.Idle -= streaming;
            }
            _streaming = true;
        }

        private void streaming(object sender, EventArgs e)
        {
            var img = capture.QueryFrame().ToImage<Bgr, Byte>();
            var bmp = img.Bitmap;
            pb_liveCamera.Image = bmp;
        }
        //Button Hover
        private void btn_TakePhoto_MouseEnter(object sender, EventArgs e)
        {
            btn_TakePhoto.Image = HotAI0._0._1.Properties.Resources.btn_takephoto_hover;
        }

        private void btn_TakePhoto_MouseLeave(object sender, EventArgs e)
        {
            btn_TakePhoto.Image = HotAI0._0._1.Properties.Resources.btn_takephoto;
        }
    }
}
