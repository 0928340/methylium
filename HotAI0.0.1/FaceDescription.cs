﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotAI0._0._1
{
    class FaceDescription
    {
        private Guid faceId;
        private int age;
        private string gender;

        public int Age { get => age; set => age = value; }
        public string Gender { get => gender; set => gender = value; }
        public Guid FaceId { get => faceId; set => faceId = value; }

        public FaceDescription(Guid faceId, int age, string gender)
        {
            this.age = age;
            this.gender = gender;
            this.faceId = faceId;
        }
    }
}
