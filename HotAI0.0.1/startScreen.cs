﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotAI0._0._1;
using HotAI;

namespace HotAI
{
    public partial class startScreen : UserControl
    {
        private static startScreen _instance;
        public static startScreen Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new startScreen();
                return _instance;
            }
        }

        public startScreen()
        {
            InitializeComponent();

        }

        public static string SetValueForScreen = "";
        public static string currentScreen = "scanScreen";

        //Hover Effects MOUSE ENTER
        private void btn_About_MouseEnter(object sender, EventArgs e)
        {btn_About.ForeColor = ColorTranslator.FromHtml("#364155");}

        private void btn_Settings_MouseEnter(object sender, EventArgs e)
        {btn_Settings.ForeColor = ColorTranslator.FromHtml("#364155");}

        private void btn_Help_MouseEnter(object sender, EventArgs e)
        {btn_Help.ForeColor = ColorTranslator.FromHtml("#364155"); }

        private void pb_btnSolo_MouseEnter(object sender, EventArgs e)
        {
            pb_btnSolo.Image = HotAI0._0._1.Properties.Resources.buttonHover;
            btn_Solo.BackColor = ColorTranslator.FromHtml("#4794c9");
        }
        private void pb_btnGroup_MouseEnter(object sender, EventArgs e)
        {
            pb_btnGroup.Image = HotAI0._0._1.Properties.Resources.buttonHover;
            btn_Group.BackColor = ColorTranslator.FromHtml("#4794c9");
        }
        private void btn_Solo_MouseEnter(object sender, EventArgs e)
        {
            pb_btnSolo.Image = HotAI0._0._1.Properties.Resources.buttonHover;
            btn_Solo.BackColor = ColorTranslator.FromHtml("#4794c9");
        }
        private void btn_Group_MouseEnter(object sender, EventArgs e)
        {
            pb_btnGroup.Image = HotAI0._0._1.Properties.Resources.buttonHover;
            btn_Group.BackColor = ColorTranslator.FromHtml("#4794c9");
        }
        //Hover Effects MOUSE LEAVE
        private void btn_About_MouseLeave(object sender, EventArgs e)
        { btn_About.ForeColor = ColorTranslator.FromHtml("#838486"); }

        private void btn_Settings_MouseLeave(object sender, EventArgs e)
        { btn_Settings.ForeColor = ColorTranslator.FromHtml("#838486"); }

        private void btn_Help_MouseLeave(object sender, EventArgs e)
        { btn_Help.ForeColor = ColorTranslator.FromHtml("#4794c9"); }
        private void pb_btnSolo_MouseLeave(object sender, EventArgs e)
        {
            pb_btnSolo.Image = HotAI0._0._1.Properties.Resources.buttonNormal;
            btn_Solo.BackColor = ColorTranslator.FromHtml("#2781bf");
        }
        private void pb_btnGroup_MouseLeave(object sender, EventArgs e)
        {
            pb_btnGroup.Image = HotAI0._0._1.Properties.Resources.buttonNormal;
            btn_Group.BackColor = ColorTranslator.FromHtml("#2781bf");
        }
        private void btn_Solo_MouseLeave(object sender, EventArgs e)
        {
            pb_btnSolo.Image = HotAI0._0._1.Properties.Resources.buttonNormal;
            btn_Solo.BackColor = ColorTranslator.FromHtml("#2781bf");
        }
        private void btn_Group_MouseLeave(object sender, EventArgs e)
        {
            pb_btnGroup.Image = HotAI0._0._1.Properties.Resources.buttonNormal;
            btn_Group.BackColor = ColorTranslator.FromHtml("#2781bf");
        }
        //Hover Effects MOUSE CLICK
        public void pb_btnSolo_Click(object sender, EventArgs e)
        {
            PageValue();
        }

        public void PageValue()
        {
            currentScreen = "scanScreen";
        }

        private void btn_Solo_Click(object sender, EventArgs e)
        {
            SetValueForScreen = "scanScreen";
            FaceScreen frm2 = new FaceScreen();
            frm2.Show();
        }

        private void pb_btnGroup_Click(object sender, EventArgs e)
        {

        }
    }
        

}

