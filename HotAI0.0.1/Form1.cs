﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using HotAI;

namespace HotAI0._0._1
{
    public partial class FaceScreen : Form
    {
        //Appconfig settings
        public FaceScreen()
        {
            InitializeComponent();
            currentScreen = "startScreen";
            loadPage();
        }
        string currentScreen = null;


        private void btn_startScreen_Click(object sender, EventArgs e)
        {
            currentScreen = "startScreen";
            loadPage();
        }

        private void btn_scanScreen_Click(object sender, EventArgs e)
        {
            currentScreen = "scanScreen";
            loadPage();
        }

        private void btn_Help_Click(object sender, EventArgs e)
        {
            currentScreen = "helpScreen";
            loadPage();
        }

        private void loadPage()
        {
            
            if (currentScreen == "startScreen")
            {
                System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString() +  " Loading Startscreen!");
                pageContainer.Controls.Add(startScreen.Instance);
                startScreen.Instance.Dock = DockStyle.Fill;
                startScreen.Instance.BringToFront();
            }
            if (currentScreen == "scanScreen")
            {
                System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString() + " Loading Scanscreen!");
                pageContainer.Controls.Add(scanScreen.Instance);
                scanScreen.Instance.Dock = DockStyle.Fill;
                scanScreen.Instance.BringToFront();
            }
            if (currentScreen == "helpScreen")
            {
                System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString() + " Loading Helpscreen!");
                pageContainer.Controls.Add(helpScreen.Instance);
                helpScreen.Instance.Dock = DockStyle.Fill;
                helpScreen.Instance.BringToFront();
            }
            if (currentScreen == "aboutScreen")
            {
                System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString() + " Loading Aboutscreen!");
                pageContainer.Controls.Add(aboutScreen.Instance);
                aboutScreen.Instance.Dock = DockStyle.Fill;
                aboutScreen.Instance.BringToFront();
            }
        }

        private void btn_About_Click(object sender, EventArgs e)
        {
            currentScreen = "aboutScreen";
            loadPage();
        }

        private void FaceScreen_Load(object sender, EventArgs e)
        {

        }
    }
}