﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotAI0._0._1
{
    static class Program
    {
        public static FaceScreen form1 = new FaceScreen(); // Place this var out of the constructor
        [STAThread]
        static void Main()
        {            
            Application.Run(form1);
        }
    }
}
