﻿namespace HotAI0._0._1
{
    partial class helpScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_title = new System.Windows.Forms.Label();
            this.btn_Help = new System.Windows.Forms.Label();
            this.btn_About = new System.Windows.Forms.Label();
            this.btn_Settings = new System.Windows.Forms.Label();
            this.lbl_groupInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HotAI0._0._1.Properties.Resources.HelpScreen;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1000, 559);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Eras Medium ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.ForeColor = System.Drawing.Color.White;
            this.lbl_title.Location = new System.Drawing.Point(462, 15);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(90, 19);
            this.lbl_title.TabIndex = 1;
            this.lbl_title.Text = "Help Menu";
            // 
            // btn_Help
            // 
            this.btn_Help.AutoSize = true;
            this.btn_Help.BackColor = System.Drawing.Color.White;
            this.btn_Help.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Help.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_Help.Location = new System.Drawing.Point(944, 533);
            this.btn_Help.Name = "btn_Help";
            this.btn_Help.Size = new System.Drawing.Size(35, 15);
            this.btn_Help.TabIndex = 5;
            this.btn_Help.Text = "Help";
            this.btn_Help.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_About
            // 
            this.btn_About.AutoSize = true;
            this.btn_About.BackColor = System.Drawing.Color.White;
            this.btn_About.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_About.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_About.Location = new System.Drawing.Point(814, 533);
            this.btn_About.Name = "btn_About";
            this.btn_About.Size = new System.Drawing.Size(44, 15);
            this.btn_About.TabIndex = 6;
            this.btn_About.Text = "About";
            this.btn_About.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_Settings
            // 
            this.btn_Settings.AutoSize = true;
            this.btn_Settings.BackColor = System.Drawing.Color.White;
            this.btn_Settings.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Settings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.btn_Settings.Location = new System.Drawing.Point(873, 533);
            this.btn_Settings.Name = "btn_Settings";
            this.btn_Settings.Size = new System.Drawing.Size(52, 15);
            this.btn_Settings.TabIndex = 7;
            this.btn_Settings.Text = "Settings";
            this.btn_Settings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_groupInfo
            // 
            this.lbl_groupInfo.AutoSize = true;
            this.lbl_groupInfo.BackColor = System.Drawing.Color.White;
            this.lbl_groupInfo.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_groupInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(132)))), ((int)(((byte)(134)))));
            this.lbl_groupInfo.Location = new System.Drawing.Point(14, 534);
            this.lbl_groupInfo.Name = "lbl_groupInfo";
            this.lbl_groupInfo.Size = new System.Drawing.Size(247, 15);
            this.lbl_groupInfo.TabIndex = 8;
            this.lbl_groupInfo.Text = "Hogeschool Rotterdam - HotAI - Project D";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(284, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Step 1: Click the camera button";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(284, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(426, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Step 2: Choose whether you want to keep the photo or delete the photo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(284, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(319, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Step 3: Take a look at the preserved recommendations";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(284, 319);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(412, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Step 4: If you want, you can filter the recommendations to your wishes";
            // 
            // helpScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(129)))), ((int)(((byte)(191)))));
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Help);
            this.Controls.Add(this.btn_About);
            this.Controls.Add(this.btn_Settings);
            this.Controls.Add(this.lbl_groupInfo);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.pictureBox1);
            this.Name = "helpScreen";
            this.Size = new System.Drawing.Size(1000, 559);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label btn_Help;
        private System.Windows.Forms.Label btn_About;
        private System.Windows.Forms.Label btn_Settings;
        private System.Windows.Forms.Label lbl_groupInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}
