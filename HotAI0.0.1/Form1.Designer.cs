﻿namespace HotAI0._0._1
{
    partial class FaceScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FaceScreen));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pageContainer = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_startScreen = new System.Windows.Forms.Button();
            this.btn_scanScreen = new System.Windows.Forms.Button();
            this.btn_About = new System.Windows.Forms.Button();
            this.btn_Help = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pageContainer
            // 
            resources.ApplyResources(this.pageContainer, "pageContainer");
            this.pageContainer.Name = "pageContainer";
            // 
            // btn_startScreen
            // 
            resources.ApplyResources(this.btn_startScreen, "btn_startScreen");
            this.btn_startScreen.Name = "btn_startScreen";
            this.btn_startScreen.UseVisualStyleBackColor = true;
            this.btn_startScreen.Click += new System.EventHandler(this.btn_startScreen_Click);
            // 
            // btn_scanScreen
            // 
            resources.ApplyResources(this.btn_scanScreen, "btn_scanScreen");
            this.btn_scanScreen.Name = "btn_scanScreen";
            this.btn_scanScreen.UseVisualStyleBackColor = true;
            this.btn_scanScreen.Click += new System.EventHandler(this.btn_scanScreen_Click);
            // 
            // btn_About
            // 
            resources.ApplyResources(this.btn_About, "btn_About");
            this.btn_About.Name = "btn_About";
            this.btn_About.UseVisualStyleBackColor = true;
            this.btn_About.Click += new System.EventHandler(this.btn_About_Click);
            // 
            // btn_Help
            // 
            resources.ApplyResources(this.btn_Help, "btn_Help");
            this.btn_Help.Name = "btn_Help";
            this.btn_Help.UseVisualStyleBackColor = true;
            this.btn_Help.Click += new System.EventHandler(this.btn_Help_Click);
            // 
            // FaceScreen
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.btn_Help);
            this.Controls.Add(this.btn_About);
            this.Controls.Add(this.btn_scanScreen);
            this.Controls.Add(this.btn_startScreen);
            this.Controls.Add(this.pageContainer);
            this.MaximizeBox = false;
            this.Name = "FaceScreen";
            this.Load += new System.EventHandler(this.FaceScreen_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel pageContainer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn_startScreen;
        private System.Windows.Forms.Button btn_scanScreen;
        private System.Windows.Forms.Button btn_About;
        private System.Windows.Forms.Button btn_Help;
    }
}

